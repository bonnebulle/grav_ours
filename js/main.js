$("html").removeClass("noscript");
// ! MD parser
// https://marked.js.org/using_advanced
marked.use({
  breaks: true,
});






window.onload = function() {

    // Mauvaise idée /vàv/ lecture retour à l'état précédent
    // scrollTo(0,0)
    
    // ! POPUPS
    const p_line_h = getComputedStyle(document.documentElement).getPropertyValue('--p_line_h');
    const popup_maxh = getComputedStyle(document.documentElement).getPropertyValue('--popup_maxh');

    setTimeout(function () {
        let hlignes = (p_line_h) ? p_line_h.replace("px", "") : "20"; // hauteur de ligne => detect if line_of_text_hover is on 1 or more lines

        // ! VAR DEBUG
        let popup_debug = "non" // 'oui' ou 'non'
        // Default
        let debug_class = (popup_debug=="oui") ? " debug" : " go";
        // ! Timer
        let delay_popup = "200"; // delay to show
        let delay_popup_start = "150"; // make it smooth : detection mouseover wait befor trigger popup
        
        // DB
        if (popup_debug==("oui")) console.log("POPUP - popup_maxh : css --popup_maxh = "+popup_maxh);
        if (popup_debug==("oui")) console.log("POPUP - delay_popup = "+delay_popup);
        if (popup_debug==("oui")) console.log("POPUP - delay_popup_start = "+delay_popup_start);
        if (popup_debug==("oui")) console.log("POPUP - hlignes : css --p_line_h = "+hlignes);

        // ? ---
        // ? POPUP Links (internals)
        // ? GEN with/by PLUGIN "markdown-yalinker" 
        // ? => [[title|lien]] / [[lien]]
        // ? OR [[title|bulle_XYZ]] / [[bulle_XYZ]] 
        // ? OR <a class="relatt_link" popup="message">text</a>
        // ? cf. user/themes/dn-corpus/templates/partials/default_article.html.twig
        $(".relatt_link").mouseover(function() {
            if ( !$(this).next().is(".popup") ) { // ! popup NOT exists
                let thisis = $(this);
                let thisid = $(this).attr("id");
                let thisptype = ($(this).attr("data-popup_type")) ? $(this).attr("data-popup_type") : "none";
                let elem = document.querySelector('#'+thisid);
                let rect = elem.getBoundingClientRect();
                for (let key in rect) {
                    if (typeof rect[key] !== "function") {
                        para = `--> ${key} : ${rect[key]}`;
                        if (key == "height") {
                           var height=rect['height'];
                           var topx=rect['x'];
                           // if (popup_debug==("oui")) alert(height);
                        }
                       
                       if (popup_debug == ("oui")) $("#wrapper").append(para), console.log(para);
                    }
                }

                // SOURCE https://stackoverflow.com/a/52477551
                // alert(window.scrollY + document.querySelector('#'+thisid).getBoundingClientRect().top)
                let ttop_pos_doc=elem.getBoundingClientRect().top;
                let left_pos_doc= elem.getBoundingClientRect().left;
                let righ_pos_doc= elem.getBoundingClientRect().right;
                let ttop_pos_popup=window.scrollY + ttop_pos_doc +'px';
                let left_pos_popup=window.scrollX + left_pos_doc +'px';
                let righ_pos_popup=window.scrollX + righ_pos_doc +'px';
                            
                let jsonurl = $(this).attr("data-url")+"?return-as=json";
                // let loaded = ( $(this).next(".loaded").length == 1 ) ? "oui" : "non";
                // if (loaded=="non") {
                    // alert("kkk")
                    $.get( jsonurl, function( data ) {;
                        
                        var message = ($(thisis).attr("popup")) ? $(thisis).attr("popup") : "non";
                        if (message=="non") {
                            var datac = data.content.replace(/===([\w\W]){1,}/,"").replace(/(\[\[.*?\]\])/g, "<span class='shortcode'>$1</span>");
                            var datamd = (datac!="") ? marked.parse(datac) : "text d'aperçu vide";    
                        } else {
                            var message = ($(thisis).attr("popup_md")=="non") ? message : marked.parse(message);
                            var datamd = message.replace(/(\[\[.*?\]\])/g, "<span class='shortcode'>$1</span>");
                        }
                        // var parentheight=$(thisis).parent().height();
                        var parentwidth=$(thisis).parent().width();
                        var thiswidth=$(thisis).width();
                        // alert(parentwidth)
                        
                        let thisptype = ($(thisis).attr("data-popup_type")) ? $(thisis).attr("data-popup_type") : "none";
                        // alert(thisptype)
                        if (thisptype=="home") {
                            var style_sizes = "min-width: "+Number(parentwidth - 30 )+"px; max-width: "+Number(parentwidth - 30 )+"px";
                        } else {
                            var style_sizes = "min-height: auto; max-width: 300px; justify-content: left; padding: 8px !important; border: 1px solid; margin-top: 1em; left: "+left_pos_popup;
                        }
                        $(thisis).after("<div style='"+style_sizes+"' data-popup_maxh='"+ popup_maxh +"' data-popup_type='"+thisptype+"' class='popup loaded"+debug_class+"'> \
                                            <span class='popup_content'>"+datamd+"</span> \
                                        </div>");
                        // } else {
                        // $(thisis).after("<div style='min-width: "+Number(parentwidth - 10 )+"px; max-width: "+Number(parentwidth - 10 )+"px' data-popup_maxh='"+ popup_maxh +"' data-popup_type='"+thisptype+"' class='popup loaded"+debug_class+"'> \
                        //                     <span class='popup_content'>"+datamd+"</span> \
                        //                 </div>");
                        // }

                        setTimeout(function () {
                            if ($(thisis).is(':hover')) {
                                $(thisis).next(".popup").addClass("active");

                        //         let windowh=window.innerHeight;
                        //         var popupheight=$(thisis).next(".popup").height();
                        //         // DB
                        //         if (popup_debug==("oui")) console.log("POP windowh = "+windowh);
                        //         if (popup_debug==("oui")) console.log("POP ttop_pos_doc = "+ttop_pos_doc);
                        //         if (popup_debug==("oui")) console.log("POP popupheight = "+popupheight);
                        //         // ! VAR
                        //         if ( (ttop_pos_doc - popupheight) < 20) {
                        //             // ! VAR
                        //             // alert("dd")
                        //             let safe_height=ttop_pos_doc % windowh - 20;
                        //             let new_style=position_finale+ "; max-height:"+safe_height+"px!important";
                                    
                        //             $(thisis).next(".popup").attr("style", new_style);
                        //             $(thisis).next(".popup").addClass("trop_grand");
                        //             $(thisis).next(".popup").find(".popup_content").attr("data-popup_maxh", safe_height+"px");
                                    
                        //             if (popup_debug == ("oui")) console.log("trop grand ! \n( ttop_pos_doc - popupheight < 100) \n==> NEW STYLE");
                        //         }
            
                            }
                        },delay_popup_start);

                    }); // ! get json
                // } else { // ? loaded
                //   console.log("already loaded")
                // }// ? loaded

            } else {
                let thisis = $(this);
                setTimeout(function () {
                    // if ($(thisis).is(':hover')) {
                        $(thisis).next(".popup").addClass("active");
                    // }
                },delay_popup);
            }
        });
        $(".relatt_link").mouseleave(function() {
            if (popup_debug == "non") {
                    $(this).next(".popup").removeClass("active");
            }
        });

    },100);
    // ! END POPUP

    
    // if (history && history.pushState) {
    //     $(function() {
    //       history.pushState(null, document.title, this.href+ev.currentTarget.hash);
    //       ev.preventDefault();
    //     });
    //   };

        $(document).ready ( function($) {
            setTimeout(function () {
            var hash= window.location.hash
            if ( hash == '' || hash == '#' || hash == undefined ) return false;
                var target = $(hash);
                headerHeight = 120;
                target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
                
                if (target.length) {
                $('html,body').stop().animate({
                    scrollTop: target.offset().top - 125 //offsets for fixed header
                }, 'linear');
            }
            



            },700);


            
        });

};


// CHECK RESIZE === observer - not works !!!
// https://developer.mozilla.org/en-US/docs/Web/API/ResizeObserver
// const divElem = document.querySelector("#wrapper");

// const resizeObserver = new ResizeObserver((entries) => {
//   for (const entry of entries) {
//     entry.target.style.width = entry.contentBoxSize[0].inlineSize + 10 + "px";
//   }
// });

// window.addEventListener("error", function (e) {
//   console.error(e.message);
// });
