# CSS home logo grid (homepage)

# NB: interactive mod for GRAV 
# Serveur => blueprints/home.yaml
==> header.ligne_1 => 6
# Admin
==> /admin/pages/home
## NB: order by folder => set at first (pages/01.home)

# Will do ==>
#container_grid {
  grid-template-areas:
    'b . . . . c' 
    'd . . . . e' 
    'f . . . . g' 
    'h i . . . p' 
    'j k . . . a' 
    'l m . . n o';
}

# NB: a == home => hide for non admins... 
# NB: need to respect the ration 6/6 (rows/col)
# NB: (.) == empty
# NB: If more contents than letters here (in css grid-template-area) => will destroy it !
# All letters preset exemple :
'b . . . x c'
'd e . . y f' 
'g a . . z w'
'h v . q p l'
'i s . u t m'
'j k . r o n'
